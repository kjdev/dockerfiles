lang en_US.UTF-8
keyboard us
timezone Asia/Tokyo --isUtc --nontp

auth --useshadow --enablemd5
network  --bootproto=dhcp --device=link --activate
firewall --disabled
selinux --disabled
rootpw --plaintext qweqwe

zerombr
clearpart --all
part / --fstype=ext4 --size=1024

bootloader --location=none

repo --name=fedora --mirrorlist=http://mirrors.fedoraproject.org/mirrorlist?repo=fedora-21&arch=$basearch
repo --name=fedora-updates --mirrorlist=http://mirrors.fedoraproject.org/mirrorlist?repo=updates-released-f21&arch=$basearch

reboot

%packages --excludedocs --nocore --instLangs=en
bash
fedora-release
vim-minimal
yum
systemd
-kernel

# removed below
passwd
firewall-config

%end

%post
echo -n "Setting default runlevel to multiuser text mode."
rm -f /etc/systemd/system/default.target
ln -s /lib/systemd/system/multi-user.target /etc/systemd/system/default.target
echo .

echo -n "Create devices which appliance-creator does not."
ln -s /proc/kcore /dev/core
mknod -m 660 /dev/loop0 b 7 0
mknod -m 660 /dev/loop1 b 7 1
rm -rf /dev/console
ln -s /dev/tty1 /dev/console
echo .

echo -n "Network fixes."
rm -f /etc/udev/rules.d/70*
ln -s /dev/null /etc/udev/rules.d/80-net-name-slot.rules

cat > /etc/hosts << EOF
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6

EOF
echo .

echo "Disabling tmpfs for /tmp."
systemctl mask tmp.mount

echo "Removing random-seed so it's not the same in every image."
rm -f /var/lib/random-seed

echo "Compressing cracklib."
gzip -9 /usr/share/cracklib/pw_dict.pwd

echo "Minimizing locale-archive."
localedef --list-archive | egrep -v 'en_US|ja_JP' | xargs localedef --delete-from-archive
mv /usr/lib/locale/locale-archive /usr/lib/locale/locale-archive.tmpl
/usr/sbin/build-locale-archive
mv /usr/share/locale/en /usr/share/locale/en_US /tmp
mv /usr/share/locale/ja /usr/share/locale/ja_JP /tmp
rm -rf /usr/share/locale/*
mv /tmp/en /tmp/en_US /usr/share/locale/
mv /tmp/ja /tmp/ja_JP /usr/share/locale/
mv /usr/share/i18n/locales/en_US /tmp
mv /usr/share/i18n/locales/ja_JP /tmp
rm -rf /usr/share/i18n/locales/*
mv /tmp/en_US /usr/share/i18n/locales/
mv /tmp/ja_JP /usr/share/i18n/locales/
echo '%_install_langs C:en:en_US:en_US.UTF-8' >> /etc/rpm/macros.imgcreate

echo "Removing extra packages."
rm -vf /etc/yum/protected.d/*
yum -C -y remove passwd --setopt="clean_requirements_on_remove=1"
yum -C -y remove firewall-config --setopt="clean_requirements_on_remove=1"

echo "Removing boot, since we don't need that."
rm -rf /boot/*

echo "Cleaning old yum repodata."
yum clean all
rm -rf /var/lib/yum/yumdb/*
truncate -c -s 0 /var/log/yum.log

echo "Zeroing out empty space."
# This forces the filesystem to reclaim space from deleted files
dd bs=1M if=/dev/zero of=/var/tmp/zeros || :
rm -f /var/tmp/zeros
echo "(Don't worry -- that out-of-space error was expected.)"

%end
