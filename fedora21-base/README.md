# Fedora 21

## Setup build tools

```
$ yum install -y appliance-tools libguestfs-tools
$ systemctl mask tmp.mount
$ reboot
```

## Build base image

[fedora-21.ks](fedora-21.ks): kickstart files.

```
$ appliance-creator -c fedora-21.ks -d -v -t /tmp -o /root/fedora --name "21" --release 21 --format=qcow2
```

## Build archive file

```
$ export LIBGUESTFS_BACKEND=direct
$ virt-tar-out -a /root/fedora/21/21-sda.qcow2 / - | xz --best > fedora-21-image.tar.xz
```

## Build Docker

Create Dockerfile.

```
$ cat > Dockerfile <<EOF
FROM scratch
MAINTAINER kjdev <kjclev@gmail.com>
ADD fedora-21-image.tar.xz /
EOF
```

Build docker.

```
$ docker build -t docker.io/kjdev/fedora-21:base .
```
