# Fedora 21 RPM Build

## Build

```
$ docker build --no-cache --force-rm=true -t docker.io/kjdev/fedora-21:rpmbuild .
```

## Run, create RPM package

```
$ mkdir SOURCES SPECS
$ # SPECS/{pkg}.spec     : package spec files
$ # SOURCES/{pkg}.tar.gz : package source files
$ docker run --rm -v $(pwd):/rpmbuild:rw -t docker.io/kjdev/fedora-21:rpmbuild
```

output: RPMS/ or SRPMS/ files.

### Dist tag

```
$ docker run -e "RPM_DIST=.fc21.kjdev" --rm -v $(pwd)/php:/rpmbuild:rw -t docker.io/kjdev/fedora-21:rpmbuild
```
