# Fedora 21 Build Pack

## Build

```
$ docker build --no-cache --force-rm=true -t kjdev/fedora-21:buildpack .
```
