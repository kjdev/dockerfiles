# Fedora 21 Phalcon Devel


## Build

```
$ docker build --no-cache --force-rm=true -t docker.io/kjdev/fedora-21:phalcon-devel .
```

## Run, execute tests

```
$ docker run --rm -it -v $(pwd):/cphalcon -t docker.io/kjdev/fedora-21:phalcon-devel
```
